#positional arguments functions
# def sum(n1,n2):
#     return n1+n2

# a=sum(10,20)
# print(a)

#Keyword arguments
# def sub(n1,n2):
#     return n1-n2

# a=sub(n2=10,n1=20)
# print(a)

def sub(n1,n2):
    return n1-n2

a=sub(10,n2=20)
print(a)

